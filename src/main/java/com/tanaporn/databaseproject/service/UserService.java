/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.databaseproject.service;

import com.tanaporn.databaseproject.dao.UserDao;
import com.tanaporn.databaseproject.model.User;

/**
 *
 * @author HP
 */
public class UserService {
    public User Login(String name, String password){
        UserDao userDao= new UserDao();
        User user = userDao.getByName(name);
        if(user != null &&user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
